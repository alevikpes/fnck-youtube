# DISCLAIMER

For YouTube, Vimeo and other services: This is a research project and 
intended to present python's great functionality.

For users and devs: Using this project may result in violation of YouTube,
Vimeo and other services TOS and other rules. This code I made for myself 
for study and research purpose and I am not responsible for anything.



# FYT Player

I try using as little of google services as possible and that results in
some troubles, because google is fncking annoying about its user tracking
and other privacy bad practices.

This project appeared as a result of big frustration of youtube. 
Every time I try opening a youtube link, which I got from somewhere, 
in a browser, I face this annoying youtube popup with consent bullshit. 
I have also noticed that it does not happen, if I open a link not in 
a browser but another application, for example Discord. So, I can 
watch almost any video from that other app without any stupid popup.

I discovered that [VLC Media Player](https://www.videolan.org/) is able 
to stream videos from internet. It also appeared that it has an API.

There is also python module for loading youtube videos called 
[pytube](https://pytube.io/en/latest/index.html). It has many features, 
which allow to stream and download youtube videos using their public 
URLs, get different streams for video and audio etc.

And it is also my first project with desktop GUI. I was trying 3 main 
packages: kyvi, tkinter and pyqt. After some time I was able to make 
it working with [PySide6](https://pypi.org/project/PySide6/) 
(the PyQt6 python wrapper).

**NOTE**: This player works only on Linux. Windows version is in development.


### Setup dev env on Linux

You need to have the following installed on your Linux:
```bash
sudo apt install vlc
```
**Note**: I was getting error without this.

Pyside6 also recommends installing `libgl-dev`:
```bash
sudo apt install libgl-dev
```
**Note**: I have not tried without it.

I use virtual environments and [pip-tools](https://pypi.org/project/pip-tools/). 
If you clone this repo, use `pip-sync` to install required python modules 
in the venv.


### Create executable

In order to create an executable for Ubuntu, I used `pyinstaller`:
```bash
pyinstaller --onedir src/main.py --name fyt-player
```
After this operation is finished, it is convenient to make a `.zip` file
with the `dist` directory to destribute around:
```bash
cd dist && zip -fr ../fyt-player.zip fyt-player && cd ..
```
The path to the executable would be:
```bash
fyt-player/fyt-player
```
In order to run the player, just unzip the archive and double-click the file
in your file manager.


### Commiting and releasing

For releases I use tags. It requires a couple of extra steps.

Firstly, make a commit in a normal way, but do not push yet.
```bash
git add .
git commit -m <Message>
```
Then, add your tag to the commit:
```bash
git tag -a fyt-player-v<version>-with-zip <commit hash>
```
You will need to add a tag comment and after this, pushing your commit will
also push the tag.

Releasing is pretty simple. Just use the functionality of your favourite
Git cloud. You will need the tag to use in the release.


## Future plans

* Unit testing.
* Sound slider is buggy.
* Separate media API from GUI.
