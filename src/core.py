from src.youtube_manager import YouTubeManager
from src.vimeo_manager import VimeoManager


class MediaManager:

    media_type = None

    def __init__(self, url, vlc_inst):
        self.vlc_inst = vlc_inst
        self.url = url

        self._media_obj = self._get_media()
        self._title = self._media_obj.title
        # length of media in milliseconds
        self._length_ms = self._media_obj.length * 1000

    @property
    def media(self):
        """Return a `vlc.Media` object."""
        return self.vlc_inst.media_new(self._media_obj.media_url)

    @property
    def title(self):
        return self._title

    @property
    def length_ms(self):
        return self._length_ms

    def get_filename(self):
        """Generate filename without spaces for saving."""
        return self._title.replace(' ', '-')

    def _get_media(self):
        if r'vimeo.com' in self.url:
            self.media_type = 'vimeo'
            media = VimeoManager(self.url)
        else:
            self.media_type = 'youtube'
            media = YouTubeManager(self.url)

        return media

    def download_video(self, filename):
        return self._media_obj.download('video', filename)

    def download_audio(self, filename):
        if isinstance(self._media_obj, YouTubeManager):
            return self._media_obj.download('audio', filename)

        else:
            raise TypeError('This functionality not supported.')

    def get_relative_position(self, media_pos: int) -> float:
        """Normalise media position and return a real number between 0 and 1."""
        pos = self._length_ms * 1000.0
        try:
            return media_pos / pos
        except ZeroDivisionError:
            raise

