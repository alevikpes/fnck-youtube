"""
This project is an extension of https://github.com/devos50/vlc-pyqt5-example.

Main differences:
* In addition to playing local media files it can stream YouTube and
  Vimeo videos.
* It uses PySide6 instead of PyQT5.
"""

import os
import pathlib
import sys
import time

# In order for `pyinstaller` to find the `vlc` external dependency, it
# is necessary to add the `vlc` plugins directory to `VLC_PLUGIN_PATH`
# environment variable (before importing `vlc`). This is only necessary
# for creating an executable. Running the player from the executable
# does not require the `vlc` to be installed on the system.
#
# This suggestion has been found here:
# https://github.com/pyinstaller/pyinstaller/issues/4506#issuecomment-585410212
os.environ['VLC_PLUGIN_PATH'] = '/usr/lib/x86_64-linux-gnu'

import vlc
from PySide6.QtCore import QTimer, Qt
from PySide6.QtGui import (
    QAction,
    QColor,
    QPalette,
)
from PySide6.QtWidgets import (
    QApplication,
    QComboBox,
    QInputDialog,
    QFileDialog,
    QHBoxLayout,
    QMainWindow,
    QMenu,
    QPushButton,
    QSlider,
    QVBoxLayout,
    QWidget,
)
from pytube import YouTube

from src.gui import GuiManager
from src.core import MediaManager

class Player(QMainWindow):
    """A simple Media Player using VLC and Qt."""

    _media_inst = None

    def __init__(self, master=None):
        QMainWindow.__init__(self, master)
        self.setWindowTitle("Media Player")

        # creating a basic vlc instance
        self.instance = vlc.Instance()
        # creating an empty vlc media player
        self.mediaplayer = self.instance.media_player_new()

        self.createUI()
        self.is_paused = False

    @property
    def gui_manager(self):
        return GuiManager(self)

    def createUI(self):
        """Set up the user interface, signals & slots."""
        widget = QWidget(self)
        self.setCentralWidget(widget)

        # set video frame
        self.videoframe = self.gui_manager.set_videoframe()

        # set video position slider
        self.positionslider = self.gui_manager.set_positionslider()
        self.positionslider.sliderMoved.connect(self.set_position)

        # set operation buttons
        self.playbutton = self.gui_manager.set_push_button('Play', self.play_pause)
        self.stopbutton = self.gui_manager.set_push_button('Stop', self.stop)

        # setup Save As menu
        self.save_as_btn = QPushButton('Save Stream As ...', self)
        self.menu = QMenu()
        self.menu.addAction('Video', self.download_video)
        self.menu.addAction('Audio', self.download_audio)
        self.save_as_btn.setMenu(self.menu)
        self.save_as_btn.setEnabled(False)

        # set volume slider
        self.volumeslider = self.gui_manager.set_volume_slider()

        # set horizontal box with button and volume slider
        hbuttonbox = QHBoxLayout()
        hbuttonbox.addWidget(self.playbutton)
        hbuttonbox.addWidget(self.stopbutton)
        hbuttonbox.addWidget(self.save_as_btn)
        hbuttonbox.addStretch(1)
        hbuttonbox.addWidget(self.volumeslider)

        # collect all boxes in one vertical layout
        vboxlayout = QVBoxLayout()
        vboxlayout.addWidget(self.videoframe)
        vboxlayout.addWidget(self.positionslider)
        vboxlayout.addLayout(hbuttonbox)
        widget.setLayout(vboxlayout)

        # set top menu
        filemenu = self.gui_manager.set_menu(
            [
                # add `Open File` menu item
                {'text': 'Open &File ...', 'action': self.play_file},
                # add `Open (YouTube, Vimeo) URL` menu item
                {'text': 'Open &URL ...', 'action': self.play_url},
                # add `Exit` menu item
                {'text': '&Exit', 'action': sys.exit},
            ],
        )

        self.timer = QTimer(self)
        self.timer.setInterval(200)
        self.timer.timeout.connect(self.update_ui)

    def play_pause(self):
        """Toggle play / pause status."""
        if self.mediaplayer.is_playing():
            self.mediaplayer.pause()
            self.playbutton.setText('Play')
            self.is_paused = True
        else:
            if self.mediaplayer.play() == -1:
                return

            self.mediaplayer.play()
            self.playbutton.setText('Pause')
            self.timer.start()
            self.is_paused = False

    def stop(self):
        """Stop player."""
        self.mediaplayer.stop()
        self.playbutton.setText('Play')
        self.positionslider.setValue(0)

    def start_media(self, media, title):
        # put the media in the media player
        self.mediaplayer.set_media(media)
        # set the title of the track as window title
        self.setWindowTitle(title)

        # The media player has to be `connected` to the `QFrame`
        # (otherwise a video would be displayed in it's own window).
        # This is platform specific!
        # You have to give the id of the `QFrame` (or similar object) to
        # `vlc`, different platforms have different functions for this.
        if sys.platform.startswith('linux'): # for Linux using the X Server
            self.mediaplayer.set_xwindow(self.videoframe.winId())
        elif sys.platform == 'win32': # for Windows
            self.mediaplayer.set_hwnd(self.videoframe.winId())
        elif sys.platform == 'darwin': # for MacOS
            self.mediaplayer.set_nsobject(int(self.videoframe.winId()))

        self.play_pause()

    def play_file(self, filename=None):
        """Open a media file in a MediaPlayer."""
        # Deactivate Download button
        self.save_as_btn.setEnabled(False)

        if not filename:
            filename = QFileDialog.getOpenFileName(
                self,
                'Open File',
                str(pathlib.Path('~').expanduser()),
            )[0]
            if not filename:
                # if open-file window is closed without choosing a file
                return

        # create the media
        media = self.instance.media_new(filename)
        title = media.get_meta(0)
        # start playing
        self.start_media(media, title)

    def play_url(self):
        url, confirmed = QInputDialog.getText(
            self,
            'Enter URL',
            'Enter URL to play:',
        )
        if not confirmed:
            # if user canceled the URL input, nothing happens
            return

        print(f'Playing URL: {url}')
        #url = 'https://www.youtube.com/watch?v=41-dLH5YqeU'
        self._media_inst = MediaManager(url, self.instance)
        media, title = self._media_inst.media, self._media_inst.title

        # Activate Download button
        self.save_as_btn.setEnabled(True)

        # start playing
        self.start_media(media, title)

    def set_volume(self, Volume):
        """Set the volume."""
        #self.volumeslider.setToolTip(f'Volume {Volume}%')
        self.mediaplayer.audio_set_volume(Volume)

    def set_position(self, position):
        """Set the position of the video."""
        # setting the position to where the slider was dragged
        self.mediaplayer.set_position(position / 1000.0)
        # the vlc MediaPlayer needs a float value between 0 and 1, Qt
        # uses integer variables, so you need a factor; the higher the
        # factor, the more precise are the results
        # (1000 should be enough)

    def update_ui(self):
        """Updates the user interface."""
        if not self.mediaplayer.is_playing():
            return

        # setting the slider to the desired position
        self.positionslider.setValue(self.mediaplayer.get_position() * 1000)

        if not self.mediaplayer.is_playing():
            # no need to call this function if nothing is played
            self.timer.stop()
            if not self.is_paused:
                # after the video finished, the play button still shows
                # "Pause", not the desired behavior of a media player
                # this will fix it
                self.stop()

    def download_video(self):
        self.save_as_btn.setText('Please Wait ...')
        self.save_as_btn.setEnabled(False)
        filename = self._get_filename_from_system(
            win_title='Save Current Video As ...',
        )
        if filename:
            time.sleep(1)
            print(f'Saving to {filename} ...')
            saved_name = self._media_inst.download_video(filename)
            print(f'Saved to {saved_name}')

        self.save_as_btn.setEnabled(True)
        self.save_as_btn.setText('Save Stream As ...')

    def download_audio(self):
        if self._media_inst.media_type == 'vimeo':
            print('ERROR: This operation is not supported for media type '
                  f'`{self._media_inst.media_type}`.')
            return

        self.save_as_btn.setText('Please Wait ...')
        self.save_as_btn.setEnabled(False)
        filename = self._get_filename_from_system(
            win_title='Save Current Audio As ...',
        )
        if filename:
            time.sleep(1)
            print(f'Saving to {filename} ...')
            saved_name = self._media_inst.download_audio(filename)
            print(f'Saved to {saved_name}')

        self.save_as_btn.setEnabled(True)
        self.save_as_btn.setText('Save Stream As ...')

    def _get_filename_from_system(self, win_title, init_path=None):
        """Opens a file dialog to either choose or create a new file
        for saving the stream into.
        """
        if not init_path:
            # Opens the dialog in the current user's directory by default.
            init_path = pathlib.Path('~').expanduser()

        # Suggest a filename as the name of the stream.
        default_name = self._media_inst.get_filename()
        filename = QFileDialog.getSaveFileName(
            self,
            win_title,
            str(init_path / default_name),
        )[0]
        if not filename:
            # If save-file window is closed without choosing a file, do nothing.
            return

        return filename

if __name__ == '__main__':
    app = QApplication(sys.argv)
    player = Player()
    player.show()
    player.resize(640, 480)
    if sys.argv[1:]:
        player.open_media(media_src=sys.argv[1])

    sys.exit(app.exec())
