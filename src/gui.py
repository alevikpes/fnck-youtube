import sys

from PySide6.QtCore import QTimer, Qt
from PySide6.QtGui import QAction, QColor, QPalette
from PySide6.QtWidgets import (
    QInputDialog,
    QFrame,
    QMenu,
    QPushButton,
    QSlider,
)


class GuiManager:

    def __init__(self, player_inst):
        self.player_inst = player_inst

    def set_videoframe(self):
        # In this widget, the video will be drawn
        if sys.platform == "darwin": # for MacOS
            from PyQt5.QtWidgets import QMacCocoaViewContainer	
            videoframe = QMacCocoaViewContainer(0)
        else:
            videoframe = QFrame()

        palette = videoframe.palette()
        palette.setColor(QPalette.Window, QColor(0,0,0))
        videoframe.setPalette(palette)
        videoframe.setAutoFillBackground(True)
        return videoframe

    def set_positionslider(self):
        slider = QSlider(Qt.Horizontal)
        slider.setToolTip("Position")
        slider.setMaximum(1000)
        #slider.sliderMoved.connect(self.set_position)
        return slider

    def set_menu(self, items: list):
        # create menu
        menubar = self.player_inst.menuBar()
        filemenu = menubar.addMenu("&File")

        # Add menu items
        for item in items:
            item = self._set_menu_item(
                item['text'],
                item['action'],
            )
            filemenu.addAction(item)

        return filemenu

    def set_push_button(self, text, action):
        button = QPushButton(text, self.player_inst)
        button.clicked.connect(action)
        return button

    def set_volume_slider(self):
        volumeslider = QSlider(Qt.Horizontal, self.player_inst)
        volumeslider.setMaximum(100)
        # set initial value
        volumeslider.setValue(50)
        # set value on change volume
        volumeslider.valueChanged.connect(self.player_inst.set_volume)
        volumeslider.setToolTip(f'Volume: {volumeslider.value()}%')
        return volumeslider

    def input_dialog(self, title, description):
        in_string, confirmed = QInputDialog.getText(
            self.player_inst,
            title,
            description,
        )
        if not confirmed:
            # if user canceled the URL input, nothing happens
            return

        return in_string

#    def save_as_btn(self, menus: dict):
#        # setup Save As button with menu
#        btn = QPushButton('Save Stream As ...')
#        menu = QMenu()
#        for item in menus:
#            if item['text'] == 'Audio':
#                action = lambda: item['action'](audio=True)
#            else:
#                action = item['action']()
#
#            menu.addAction(item['text'], action)
#
#        btn.setMenu(menu)
#        return btn

    def _set_menu_item(self, text, action):
        # add menu item
        item = QAction(text, self.player_inst)
        item.triggered.connect(action)
        return item
