import logging
import pathlib

import vlc
from pytube import YouTube


logger = logging.getLogger(__name__)


class YouTubeManager:

    def __init__(self, url: str, hq: bool=True):
        self.url = url
        self._yt = YouTube(self.url)
        self._streams = self._yt.streams
        #self._curr_stream = choose_video_quality()

    @property
    def media_url(self, hq=True):
        """Return a the URL of the file to play / download.

        :param hq: - Gets the heighest quality video by default (hq=True).
        """
        vid = self.choose_video_quality(hq=hq)
        return vid.url

    @property
    def title(self):
        return self._yt.title

    @property
    def length(self):
        """Gets the length of the media in seconds."""
        return int(self._yt.length)

    #def show_mime_types(self) -> list:
    #    return [s.mime_type for s in self._streams]

    #def get_mime_type(self, mime_type_str: str) -> list:
    #    return [s for s in self._streams if s.mime_type == mime_type_str]

    def choose_video_quality(self, hq=True, mobile=False):
        """Choose the quality of the video.

        :param hq: - Gets the heighest quality video by default (hq=True).
        """
        vids = self._streams.filter(progressive=True, file_extension='mp4')
        if mobile:
            vids = self._streams.filter(progressive=True, file_extension='3gpp')

        if hq:
            vid = vids[-1]
        else:
            vid = vids[0]

        #self._curr_stream = vid
        return vid

    def choose_audio_quality(self, hq=True):
        """Choose the quality of the audio.

        :param hq: - Gets the heighest quality audio by default (hq=True).
        """
        auds = self._streams.filter(only_audio=True)
        if hq:
            aud = auds[-1]
        else:
            aud = auds[0]

        return aud

    # FIXME: Include quality parameter
    def download(self, stream_type, filename):
        filename = pathlib.PurePath(filename)
        if stream_type == 'video':
            stream = self.choose_video_quality()
            #filename = filename.name.split('.')[-2]
        elif stream_type == 'audio':
            stream = self.choose_audio_quality()
        else:
            raise TypeError(
                f'Error: Stream type `{stream_type}` not supported!')

        ext = stream.default_filename.split('.')[-1]
        return stream.download(filename.parent, f'{filename.name}.{ext}')
