import logging
from operator import itemgetter

import requests
import vlc

from src.generate_headers import HeadersGenerator


logger = logging.getLogger(__name__)


class VimeoManager:

    def __init__(self, url: str):
        self._config_url = self._get_config_url(url)
        self._config = self._get_config()
        self._file_url = self._choose_quality()

    @property
    def media_url(self, hq=True):
        """Return the URL of the file to play / download.

        :param hq: - Gets the heighest quality video by default (hq=True).
        """
        return self._file_url

    @property
    def title(self):
        return self._config.get('video', {}).get('title', 'Could not get title')

    @property
    def length(self):
        """Gets the length of the media in seconds."""
        return int(self._config.get('video', {}).get('duration', 0))

    def download_video(self, filename):
        hdr = HeadersGenerator()
        headers = hdr.generate_random_headers()
        resp = requests.get(self._file_url, headers=headers)
        with open(filename, 'wb') as f:
            f.write(resp.content)

        return filename

    def _get_config_url(self, url):
        split_url = url.split('/')
        v_id = split_url[-1] if split_url[-1] else split_url[-2]
        return f'https://player.vimeo.com/video/{v_id}/config'

    def _get_config(self):
        """Get data from the config page of the Vimeo video.

        The user URL has format `https://vimeo.com/<id>/`, so the config URL
        is `https://player.vimeo.com/video/<id>/config`. It contains all
        the metadata in json format.
        """
        hdr = HeadersGenerator()
        headers = hdr.generate_random_headers()
        resp = requests.get(self._config_url, headers=headers)
        return resp.json()

    def _get_files(self):
        """Get data from the config page of the Vimeo video.

        The user URL has format `https://vimeo.com/<id>/`, so the config URL
        is `https://player.vimeo.com/video/<id>/config`. It contains all
        the metadata in json format.
        """
        # Get all `mp4` files.
        return self._config.get('request', {}).get('files', {}).get('progressive')

    def _choose_quality(self, hq=True):
        """Choose quality of the video.

        :param hq: - Gets the heighest quality video by default (hq=True).
        """
        # Get highest quality.
        files = self._get_files()
        f_sorted = sorted(files, key=itemgetter('height'))
        f = f_sorted[-1]
        if not hq:
            f = f_sorted[0]

        return f['url']
